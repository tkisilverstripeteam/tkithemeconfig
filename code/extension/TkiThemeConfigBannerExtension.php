<?php

class TkiThemeConfigBannerExtension extends DataExtension {

	private static $db = array(
		'BannerSource' => "Enum('default,inherit,custom,none','default')"
	);

	private static $has_one = array(
		'BannerImage' => 'Image'
	);

	public function updateCMSFields(FieldList $fields)
	{
		// Header
		$headerField = HeaderField::create('BannerHeader',
			_t('TkiThemeConfigBannerSiteTreeExtension.BannerImage', 'Static banner image'));

		/*
		 * Inherit
		 */
		$parent = $this->findParentWithBannerImage();

		if($parent) {
			$parentImageUrl = $parent->BannerImage()->setWidth(300)->URL;
			$parentData = '<img src="'. $parentImageUrl .'" alt="Inherited banner image" />';
		} else {
			$parentData = '<p>'. _t('TkiThemeConfigBannerSiteTreeExtension.NoInherited', 'No inherited image') .'</p>';
		}
		$inheritField = LiteralField::create('BannerImageInherit',$parentData);

		/*
		 * Default
		 */
		$default = $this->findDefaultBannerImage();
		if($default) {
			$defaultUrl = $default->setWidth(300)->URL;
			$defaultData = '<img src="'. $defaultUrl .'" alt="'. _t('TkiThemeConfigBannerSiteTreeExtension.Default', 'Default banner image') .'" />';
		} else {
			$defaultData = '<p>'. _t('TkiThemeConfigBannerSiteTreeExtension.NoDefault', 'No default image') .'</p>';
		}
		$defaultField = LiteralField::create('BannerImageDefault',$defaultData);

		/*
		 *  Custom
		 */
		$folder = null;
		$siteConfig = SiteConfig::current_site_config();
		if($siteConfig->hasExtension('TkiThemeConfigSiteConfigExtension')) {
			$siteConfig->initDefaultUploadsFolder();
		}
		$customField = UploadField::create('BannerImage',_t('TkiThemeConfigBannerSiteTreeExtension.SelectImage', 'Select Image'));

		/*
		 * None
		 */
		$noneField = LiteralField::create('BannerImageNone',
			'<p>'. _t('TkiThemeConfigBannerSiteTreeExtension.NoImage', 'No image') .'</p>');

		/*
		 * Selection field
		 */
		$options = array(
			SelectionGroup_Item::create('default',$defaultField, _t('TkiThemeConfigBannerSiteTreeExtension.SourceDefault')),
			SelectionGroup_Item::create('inherit',$inheritField, _t('TkiThemeConfigBannerSiteTreeExtension.SourceInherit')),
			SelectionGroup_Item::create('custom',$customField, _t('TkiThemeConfigBannerSiteTreeExtension.SourceCustom')),
			SelectionGroup_Item::create('none',$noneField, _t('TkiThemeConfigBannerSiteTreeExtension.SourceNone'))
		);

		$selectionField = SelectionGroup::create('BannerSource', $options);
		$selectionField->setValue('default');
		$fields->addFieldsToTab('Root.Banner',array($headerField,$selectionField));

	}

	/**
	 * Finds default banner image, set in site config
	 * @return Image|null
	 */
	protected function findDefaultBannerImage() {
		$image = null;
		if(class_exists('SiteConfig')) {
			$siteConfig = SiteConfig::current_site_config();
			$image = (!empty($siteConfig->BannerImageID)) ? $siteConfig->BannerImage() : null;
		}
		return $image;
	}

	/**
	 * Finds closest parent with a banner image
	 * @return SiteTree
	 */
	protected function findParentWithBannerImage() {
		$parent = $this->owner->Parent;
		while($parent) {
			if(!empty($parent->BannerImageID)) {
				return $parent;
			}
			$parent = $parent->Parent;
		}
		return null;
	}

	/**
	 * Returns Banner image object for template
	 * @return Image|null
	 */
	public function BannerImageObj() {
        if($this->owner->BannerImageCached) {
            return $this->owner->BannerImageCached;
        }
		$image = null;

		// Get configured banner dimensions
		$width = null;
		$height = null;

		if(class_exists('SiteConfig')) {
			$siteConfig = SiteConfig::current_site_config();
			$width = (!empty($siteConfig->BannerWidth)) ? $siteConfig->BannerWidth : null;
			$height = (!empty($siteConfig->BannerHeight)) ? $siteConfig->BannerHeight : null;
		}

		switch($this->owner->BannerSource) {
			case 'inherit':
				$parent = $this->findParentWithBannerImage();
				$image = ($parent) ? $parent->BannerImage() : null;
				break;
			case 'custom':
				$image = $this->owner->BannerImage();
				break;
			case 'none':
				break;
			case 'default':
			default:
				$image = $this->findDefaultBannerImage();
				break;
		}
        
        $this->owner->BannerImageCached = $image;
        
		if($image && $width && $height) {
			return $image->Fill($width,$height);
		} else {
			return $image;
		}
	}


}
