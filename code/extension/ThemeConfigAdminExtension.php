<?php

/**
 * @deprecated
 */
class ThemeConfigAdminExtension extends Extension
{
	
	/**
	 * Sets default uploads folder to the folder configured in site config.
	 * @deprecated - This likely causes a reduction in performance
	 */
	public function onAfterInit()
    {
		// First check if SiteConfig is present
		if(!class_exists('SiteConfig')) return;

		$siteConfig = SiteConfig::current_site_config();
		if($siteConfig && !empty($siteConfig->UploadsFolderID)) {
			$uploadsFolder = $siteConfig->UploadsFolder()->Filename;
			// Remove assets folder name from upload folder path
			if(stripos($uploadsFolder, ASSETS_DIR) === 0) {
				$uploadsFolder = substr($uploadsFolder, strlen(ASSETS_DIR) + 1);
			}
			Config::inst()->update('Upload', 'uploads_folder', $uploadsFolder);
		}
		
		
	}
	
	
}
		


