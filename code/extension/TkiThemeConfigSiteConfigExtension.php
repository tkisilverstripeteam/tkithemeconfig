<?php

class TkiThemeConfigSiteConfigExtension extends DataExtension {

	private static $db = [
		'Subtitle' => 'Varchar(255)',
		'Copyright' => 'Varchar(255)',
		'Credit' => 'Varchar(255)',
		'CreditURL' => 'Varchar(255)',
		'GACode' => 'Varchar(16)',
		'FacebookURL' => 'Varchar(255)',
		'YouTubeURL' => 'Varchar(255)',
		'BannerWidth' => 'Int',
		'BannerHeight' => 'Int'
	];

	private static $has_one = array(
		'BannerImage' => 'Image',
		'UploadsFolder' => 'Folder'
	);

	public function updateCMSFields(FieldList $fields)
    {
		/* ---- Main ---- */
		$fields->addFieldToTab('Root.Main',
			TextField::create('GACode', _t('TkiThemeConfigSiteConfigExtension.GACode','Google Analytics account code'))
		);

		// Subtitle
		$fields->addFieldToTab('Root.Main',TextField::create('Subtitle', _t('TkiThemeConfigSubsiteExtension.Subtitle', 'Subtitle')),'Tagline');
		$this->initDefaultUploadsFolder();
		// Uploads folder
		$uploadsField = TreeDropdownField::create('UploadsFolderID',_t('TkiThemeConfigSiteConfigExtension.UploadsFolder', 'Uploads folder'),'Folder');
		$fields->addFieldToTab('Root.Main',$uploadsField,'Theme');
		// Legal
		$legalFields = array(
			HeaderField::create('HeaderLegal',_t('TkiThemeConfigSiteConfigExtension.HeaderCopyright', 'Copyright')),
			TextField::create('Copyright', _t('TkiThemeConfigSubsiteExtension.Copyright', 'Copyright')),
			TextField::create('Credit', _t('TkiThemeConfigSubsiteExtension.Credit', 'Credit')),
			TextField::create('CreditURL', _t('TkiThemeConfigSubsiteExtension.CreditURL', 'Credit URL')),
		);
		$fields->addFieldsToTab('Root.Main',$legalFields);

		/* ---- Social ---- */
		$fields->findOrMakeTab(
			'Root.SocialMedia'
		);
		$socialFields = array(
			TextField::create('FacebookURL', _t('TkiThemeConfigSubsiteExtension.FacebookURL', 'Facebook URL')),
			TextField::create('YouTubeURL', _t('TkiThemeConfigSubsiteExtension.YouTubeURL', 'YouTube URL'))
		);

		$fields->addFieldsToTab('Root.SocialMedia',$socialFields);

		/* ---- Images ---- */
		$fields->findOrMakeTab('Root.Images',_t('TkiThemeConfigSiteConfigExtension.ImagesTab'));
		// Banner
		$bannerFields = array(
			HeaderField::create('HeaderBanner',_t('TkiThemeConfigSiteConfigExtension.HeaderBanner', 'Banner')),
			UploadField::create('BannerImage',_t('TkiThemeConfigSiteConfigExtension.BannerImage', 'Banner Image')),
			NumericField::create('BannerWidth',_t('TkiThemeConfigSiteConfigExtension.BannerWidth', 'Banner Width')),
			NumericField::create('BannerHeight',_t('TkiThemeConfigSiteConfigExtension.BannerHeight', 'Banner Height'))
		);
		$fields->addFieldsToTab('Root.Images',$bannerFields);

    }

	public function setCreditURL($value)
	{
		$this->owner->setField('CreditURL',$this->prepareURL($value));
	}
	
	public function setFacebookURL($value)
	{
		$this->owner->setField('FacebookURL',$this->prepareURL($value));
	}
	
	public function setYouTubeURL($value)
	{
		$this->owner->setField('YouTubeURL',$this->prepareURL($value));
	}

	/**
	 * Adds scheme if missing
	 * @param  string $value
	 * @return string
	 */
	protected function prepareURL($value)
	{
		$scheme = parse_url($value,PHP_URL_SCHEME);
		if(!empty($value) && (empty($scheme) || strpos($value,'://' === false))) {
			$value = 'http://'. $value;
		}
		return $value;
	}
	
	public function initDefaultUploadsFolder()
	{
		if(!empty($this->owner->UploadsFolderID)) {
			$uploadsFolder = $this->owner->UploadsFolder()->Filename;
			// Remove assets folder name from upload folder path
			if(stripos($uploadsFolder, ASSETS_DIR) === 0) {
				$uploadsFolder = substr($uploadsFolder, strlen(ASSETS_DIR) + 1);
			}
			Config::inst()->update('Upload', 'uploads_folder', $uploadsFolder);
		}
	}
	
}
