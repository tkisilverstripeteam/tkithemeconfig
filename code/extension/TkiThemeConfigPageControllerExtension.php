<?php

class TkiThemeConfigPageControllerExtension extends Extension {

	/*
	 * -------------------------------------------------------------------------
	 * Controller methods
	 * -------------------------------------------------------------------------
	 */


	public function contentcontrollerInit($controller) {
		$themeService = Injector::inst()->get('ThemeConfigService');
		$themeService->requireConfiguredLibs();
	}


	/*
	 * -------------------------------------------------------------------------
	 * Template methods
	 * -------------------------------------------------------------------------
	 */


	public function isDraft() {
		return (Versioned::current_stage() === 'Stage');
	}

	public function isPublished() {
		return (Versioned::current_stage() === 'Live');
	}

	public function showDraftNotice() {
		return true;
	}

	public function isNotAdminUrl() {
		$out = true;
		if(!empty($_GET['BackURL'])) {
			$url_parts = explode('/',$_GET['BackURL']);
			$out = !in_array('admin',$url_parts);
		}
		return $out;
	}

	public function IsLiveMode() {
		return Director::isLive();
	}

	public function IsDevMode() {
		return Director::isDev();
	}

	public function IsTestMode() {
		return Director::isTest();
	}

	public function LinkByID($id)
	{
		$page = Page::get()->byID($id);
		return ($page) ? $page->AbsoluteLink() : '';
	}

	public function CurrentYear()
	{
		return (string) date('Y');
	}


}
