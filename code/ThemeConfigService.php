<?php

class ThemeConfigService extends Object {


	/**
	 * Default global css requirements
	 * @var array
	 * @config
	 */
	private static $css = [];

	/**
	 * Default global js requirements
	 * @var array
	 * @config
	 */
	private static $js = [];

	/**
	 * @var array
	 * @config
	 */
	private static $themes = [];

	/*
	 * -------------------------------------------------------------------------
	 * Configuration methods
	 * -------------------------------------------------------------------------
	 */

	public function getGlobalCss()
	{
		return $this->config()->get('css');
	}

	public function getThemeCss()
	{
		return $this->themeConfigByClass($this,'css');
	}

	public function getGlobalJs()
	{
		return $this->config()->get('js');
	}

	public function getThemeJs()
	{
		return $this->themeConfigByClass($this,'js');
	}

	/**
	 * Gets current theme
	 */
	public function currentTheme()
	{
		$theme = Config::inst()->get('SSViewer', 'theme');
		if (!$theme && class_exists('SiteConfig')) {
			$theme = SiteConfig::current_site_config()->Theme;
		}
		return $theme ? $theme : 'default';
	}

	/**
	 * Gets current theme config for the active class,
	 * optionally returns specific value if key provided
	 * @param string $theme
	 * @return array
	 */
	public function themeConfigByClass($class,$key=null)
	{
		$className = (is_object($class)) ? get_class($class) : $class;
		$theme = $this->currentTheme();
		$config = Config::inst()->get($className,'themes');
		$themeConfig = ($theme && isset($config[$theme])) ? $config[$theme] : [];
		if(!empty($value)) {
			return isset($themeConfig[$value]) ? $themeConfig[$value] : null;
		} else {
			return $themeConfig;
		}
	}

	/*
	 * -------------------------------------------------------------------------
	 * Require methods
	 * -------------------------------------------------------------------------
	 */

	public function requireConfiguredLibs() {
		$require = Requirements::backend();
		if($require instanceof TkiRequirements_Backend) {

			$require->stylesheets($this->getGlobalCss());
			$require->stylesheets($this->getThemeCss());
			// JS
			$globalJs = $this->getGlobalJs();

			if($globalJs) {
				$require->js(array('global' => $globalJs),'body',80);
			}

			$themeJs = $this->getThemeJs();
			if($themeJs) {
				$theme = $this->currentTheme();
				$require->js(array($theme => $themeJs),'body',70);
			}


		}


	}




}
